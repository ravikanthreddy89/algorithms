import java.util.Iterator;


public class Stack<Item> implements Iterable<Item> {

	private Node first=null;
	private static int size;
	private class Node {
		Item item;
		Node next;
	}
	
	public Iterator<Item> iterator() {
	   return new StackIterator();	
	}
	private class StackIterator implements Iterator<Item> {
		
		Node current=first;
		public boolean hasNext(){
			return current!=null;
		}
		public Item next(){
			Item it= current.item;
			current=current.next;
			return it;
		}
		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
	
	public void push(Item item){
		Node oldfirst= first;
		first= new Node();
		first.item= item;
		first.next=oldfirst;
	    size=size+1;	
	 }
	
	public Item pop() {
		Item it=null;
		if(first==null) it=null;
		else {
			it=first.item;
			first=first.next;
			size=size-1;
		}
		return it;
	}
}
