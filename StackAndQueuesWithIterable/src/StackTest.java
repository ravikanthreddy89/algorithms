import java.util.Iterator;
import java.util.Scanner;


public class StackTest {

		public static void main(String [] args){
			Stack stk= new Stack();
			Scanner input= new Scanner(System.in);
			System.out.println("Pushing number 0-9 onto the stack");
			for(int i=0; i< 10 ; i++ ) stk.push(i);
			System.out.println("Fetching the iterator");
			Iterator itr= stk.iterator();
			System.out.println("Iterating over the stack");
			while(itr.hasNext()==true){
				System.out.println(itr.next());
			}
			System.out.println("Test passed");
	}
}
