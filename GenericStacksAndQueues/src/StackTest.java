import java.util.Scanner;


public class StackTest {

	public static void main(String [] args){
		Stack stk= new Stack();
		Scanner input= new Scanner(System.in);
		while(true){
			String user_input= input.nextLine();
			/* Push command */
			if(user_input.split(" ")[0].equals("push")){
				stk.push(user_input.split(" ")[1]);
			}
			/*  Pop command */
			else if(user_input.split(" ")[0].equals("pop")){
				int size= stk.size();
				if(size==0) System.out.println("Stack is empty");
				else System.out.println(stk.pop());
			}
			/* Size command */
			else if(user_input.split(" ")[0].equals("size")){
				System.out.println(stk.size());
			} 
			/* Quit command */
			else if(user_input.split(" ")[0].equals("quit")){
				break;
			}else System.out.println("unknown command");
		}
		System.out.println("Exiting...bye!!!");
	}
}
