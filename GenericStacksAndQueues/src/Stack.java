
public class Stack<Item> {

	private class Node {
		Item item;
		Node next;
	}
	
	private Node first=null;
	private static int size;
	
	public void push(Item item){
		Node oldfirst= first;
		first= new Node ();
		first.item=item;
		first.next=oldfirst;
		size=size+1;
	}
	
	public Item pop(){
		Item ret;
		if(first==null) ret=null;
		else{
			ret=first.item;
			first=first.next;
			size=size-1;
		}
		return ret;
	}
	
	public int size() {
		return size;
	}
}
