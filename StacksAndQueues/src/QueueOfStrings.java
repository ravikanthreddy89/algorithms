
public class QueueOfStrings {

	class Node {
		String value;
		Node next;
		}
	Node first=null;
	Node last=null;
	int size=0;
	
	public void enqueue(String s){
		if(first==null){
			first= new Node();
			first.value=s;
			first.next=null;
			last=first;
		}
		else {
			last.next=new Node();
			last.next.value=s;
			last.next.next=null;
			last=last.next;
		}
		size=size+1;
	}
	
	public String dequeue(){
		String ret;
	    if(first==null) ret= null;
	    else {
	    	ret=first.value;
	    	first=first.next;
	    	size=size-1;
	    }
	    return ret;
	}
	
	public int size() {
		return size;
	}
}
