
public class StackOfStrings {

	static int size;
	Node first=null;
	class Node {
		String value;
		Node next;
	}
	
	public void push(String s){
		Node oldfirst= first;
		first = new Node();
		first.value = s;
		first.next= oldfirst;
		size=size+1;
	}
	
	public String pop(){
		String ret=null;
		if(first==null) ret=null;
		else {
			ret=first.value;
			first=first.next;
			size=size-1;
		}
		
		return ret;
	}
	
	public int size() {
		return size;
	}
}
