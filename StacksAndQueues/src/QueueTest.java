import java.util.Scanner;


public class QueueTest {

	public static void main(String [] args){
		QueueOfStrings qst= new QueueOfStrings();
		Scanner input = new Scanner(System.in);
		while(true){
			
				String user_input= input.nextLine();
				String[] user_input_split= user_input.split(" ");
				
				if(user_input_split[0].equals("enq")){
					qst.enqueue(user_input_split[1]);
				}else if(user_input_split[0].equals("deq")){
					String temp= qst.dequeue();
							if (temp==null) System.out.println("Q is empty");
							else System.out.println(temp);
					
				} else if(user_input_split[0].equals("size")){
					System.out.println(qst.size());
				} else if(user_input_split[0].equals("quit")){
					break;
				}
			}
	}
}
